<?php
	include dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Categorias2
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos los usuarios registrados
		public function getCategorias2()
		{
			$query  ="SELECT * FROM categorias2";
			$result =mysqli_query($this->link,$query);
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}

		//Crea un nuevo usuario
		public function newCategoria2($data){
			$query  ="INSERT INTO categorias2 (nombre) VALUES ('".$data['nombre']."')";
			$result =mysqli_query($this->link,$query);
			if(mysqli_affected_rows($this->link)>0){
				return true;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function getCategoria2ById($id=NULL){
			if(!empty($id)){
				$query  ="SELECT * FROM categorias2 WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function setEditcategoria2($data){
			if(!empty($data['id'])){
				$query  ="UPDATE categorias2 SET nombre='".$data['nombre']."' WHERE id=".$data['id'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra el usuario por id
		public function deletecategoria2($id=NULL){
			if(!empty($id)){
				$query  ="DELETE FROM categorias2 WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Filtro de busqueda
		public function getcategorias2BySearch($data=NULL){
			if(!empty($data)){
				$query  ="SELECT * FROM categorias2 WHERE nombre LIKE'%".$data."%'";
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}
	}