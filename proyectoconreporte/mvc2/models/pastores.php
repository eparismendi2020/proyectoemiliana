<?php
	include dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Pastores
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos los usuarios registrados
		public function getPastores()
		{
			$query  ="SELECT * FROM pastores";
			$result =mysqli_query($this->link,$query);
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}

		//Crea un nuevo usuario
		public function newPastore($data){
			$query  ="INSERT INTO pastores (identificacion, descripcion, monto) VALUES ('".$data['identificacion']."','".$data['descripcion']."','".$data['monto']."')";
			$result =mysqli_query($this->link,$query);
			if(mysqli_affected_rows($this->link)>0){
				return true;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function getPastoreById($id=NULL){
			if(!empty($id)){
				$query  ="SELECT * FROM pastores WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function setEditPastore($data){
			if(!empty($data['id'])){
				$query  ="UPDATE pastores SET identificacion='".$data['identificacion']."',descripcion='".$data['descripcion']."', monto='".$data['monto']."' WHERE id=".$data['id'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra el usuario por id
		public function deletePastore($id=NULL){
			if(!empty($id)){
				$query  ="DELETE FROM pastores WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Filtro de busqueda
		public function getPastoresBySearch($data=NULL){
			if(!empty($data)){
				$query  ="SELECT * FROM pastores WHERE identificacion LIKE'%".$data."%' OR descripcion LIKE'%".$data."%' OR monto LIKE'%".$data."%'";
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}
	}