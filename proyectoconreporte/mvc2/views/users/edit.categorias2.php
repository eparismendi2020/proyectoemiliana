<?php

  include './models/categorias2.php';
  $title="Listado de Usuarios";

  $categoria2     = new Categorias2();
  $id       = isset($_GET['id'])?$_GET['id']:null;
  $categorias2    = $categoria2->getCategoria2ById($id);
  $nombre     = '';

 
  if($categorias2){
    $nombre     =$categorias2[0]['nombre'];

  
  }

	include 'toolbar.categorias2.php';
?>
<form action="./controllers/categorias2_controller.php" method="POST">
  <div class="form-group">
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Tus nombre" autofocus required value="<?php echo $nombre; ?>">
  </div>

  
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>