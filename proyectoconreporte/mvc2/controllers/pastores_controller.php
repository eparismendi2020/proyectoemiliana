
<?php
	include dirname(__file__,2).'/models/pastores.php';

	$pastores=new Pastores();

	//Request: creacion de nuevo usuario
	if(isset($_POST['create']))
	{
		if($pastores->newPastore($_POST)){
			header('location: ../pastores.php?page4=new.pastores&success=true');
		}else{
			header('location: ../pastores.php?page4=new.pastores&error=true');
		}
	}

	//Request: editar usuario
	if(isset($_POST['edit']))
	{
		if($pastores->setEditPastore($_POST)){
			header('location: ../pastores.php?page4=edit.pastores&id='.$_POST['id'].'&success=true');
		}else{
			header('location: ../pastores.php?page4=edit.pastores&id='.$_POST['id'].'&error=true');
		}
	}

	//Request: editar usuario
	if(isset($_GET['delete']))
	{
		if($pastores->deletePastore($_GET['id'])){
			// header('location: ../pastores.php?page4=pastores&success=true');
			echo json_encode(["success"=>true]);
		}else{
			// header('location: ../pastores.php?page4=pastores&&error=true');
			echo json_encode(["error"=>true]);
		}
	}

?>