<?php

require('fpdf/fpdf.php');



class PDF extends FPDF
{
    // Cabecera de p�gina
    function Header()
    {
        // Logo
        $this->Image('https://i.pinimg.com/236x/6e/c7/b8/6ec7b8e2a4ba28b2d28280fc9421f8dc--church.jpg',10,8,40);
        // Salto de l�nea
        $this->Ln(20);
        // Arial bold 15
        $this->SetFont('Arial','B',18);
        // Movernos a la derecha
        $this->Cell(60);
        // T�tulo
        $this->Cell(80,10,'REPORTES DE INGRESOS',0,0,'C');
        // Salto de l�nea
        $this->Ln(20);
        $this->Cell(50, 10, 'Identificacion', 1, 0, 'C', 0);
        $this->Cell(50, 10, 'Nombre', 1, 0, 'C', 0);
        $this->Cell(30, 10, 'Categoria ', 1, 0, 'C', 0);
        $this->Cell(30, 10, 'Fecha', 1, 0, 'C', 0);
       
        $this->Cell(30, 10, 'Monto', 1, 1, 'C', 0);

    }
    
    // Pie de p�gina
    
    function Footer()
    {
        

      
      
        
        // Posici�n: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // N�mero de p�gina
        $this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'C');
    }
}













require 'config/cn.php';
$consulta = "SELECT ingresos.id,ingresos.identificacion,ingresos.descripcion,ingresos.monto, ingresos.fecha,
            ingresos.categorias_id,categorias.nombre FROM ingresos,categorias WHERE categorias.id=ingresos.categorias_id ";



$resultado = $mysqli->query($consulta);





$pdf = new PDF();
$pdf-> AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',16);


$count=0;
while ($row = $resultado->fetch_assoc()){
    $pdf->Cell(50, 10, $row['identificacion'], 1, 0, 'C', 0);
    $pdf->Cell(50, 10, $row['descripcion'], 1, 0, 'C', 0);
    $pdf->Cell(30, 10, $row['nombre'], 1, 0, 'C', 0);
    $pdf->Cell(30, 10, $row['fecha'], 1, 0, 'C', 0);
   
    $pdf->Cell(30, 10, $row['monto'], 1, 1, 'C', 0);
   
    $count += $row['monto'];
    
  
   
}

$pdf->Cell(130);
$pdf->Cell(30, 10, 'Total ', 1, 0, 'C', 0);




$pdf->Cell(30, 10, $count, 1, 1, 'C', 0);



$pdf->Output();



?>