<?php
	include 'toolbar.egresos.php';
	$db=new PDO("mysql:host=localhost;dbname=crud2;charset=utf8","root","");
	$filas=$db->query("SELECT * FROM `categorias2`")->fetchAll(PDO::FETCH_OBJ); ?>

<form action="./controllers/egresos_controller.php" method="POST">
  <div class="form-group">
  
  	 <label for="identificacion">Identificacion</label>
    <input type="text" class="form-control" id="identificacion" name="identificacion" autofocus placeholder="Tu identificacion" required>
  </div>
  <div class="form-group">
  	 <label for="descripcion">Nombre</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Tu nombre" required>
  </div>
  <div class="form-group">
  	 <label for="monto">Monto</label>
    <input type="text" class="form-control" id="monto" name="monto" placeholder="Tu monto" required>
  </div>
  
    <div class="form-group">
  	 <label for="fecha">Fecha</label>
    <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Tu fecha" required>
  </div>
  

  
  <div class="form-group">
				    <label class="col-sm-2 control-label">Tipo de Egreso</label>
				    <div class="col-sm-10">

<select name="categorias2_id" class="form-control">
				      	<option value="">Selecione un tipo de Egreso</option>
				      	<?php foreach ($filas as $filas): ?>
				      		<option value="<?php echo $filas->id; ?>"><?php echo $filas->nombre; ?></option>
				      	<?php endforeach ?>
				      </select>
</div>
				  </div>
  
  
  
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				El egreso ha sido creado.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear el egreso, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
</form>