<?php
	include 'toolbar.categorias.php';
?>
<form action="./controllers/categorias_controller.php" method="POST">
  <div class="form-group">
  
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre" autofocus placeholder="Tu nombre" required>
  </div>
  <div class="form-group">

  
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La categoria ha sido creado.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear la categoria, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
</form>