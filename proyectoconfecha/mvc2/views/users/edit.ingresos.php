<?php

  include './models/ingresos.php';
  $title="Listado de Usuarios";

  $db=new PDO("mysql:host=localhost;dbname=crud2;charset=utf8","root","");
  $filas=$db->query("SELECT * FROM `categorias`")->fetchAll(PDO::FETCH_OBJ); 

  $ingreso     = new Ingresos();
  $id       = isset($_GET['id'])?$_GET['id']:null;
  $ingresos    = $ingreso->getIngresoById($id);
  $identificacion     = '';
  $descripcion = '';
  $monto    = '';
  $fecha    = '';
  $categorias_id    = '';
 
  if($ingresos){
    $identificacion     =$ingresos[0]['identificacion'];
    $descripcion =$ingresos[0]['descripcion'];
    $monto    =$ingresos[0]['monto'];
    $fecha    =$ingresos[0]['fecha'];
    $categorias_id    =$ingresos[0]['categorias_id'];
  
  }

	include 'toolbar.ingresos.php';
?>
<form action="./controllers/ingresos_controller.php" method="POST">
  <div class="form-group">
  	 <label for="identificacion">Identificacion</label>
    <input type="text" class="form-control" id="identificacion" name="identificacion" placeholder="Tus identificacion" autofocus required value="<?php echo $identificacion; ?>">
  </div>
  <div class="form-group">
  	 <label for="descripcion">Nombre</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Tus descripcion" required value="<?php echo $descripcion; ?>">
  </div>
  <div class="form-group">
  	 <label for="monto">Monto</label>
    <input type="text" class="form-control" id="monto" name="monto" placeholder="Tu monto" required value="<?php echo $monto; ?>">
  </div>
  
  <div class="form-group">
  	 <label for="fecha">Fecha</label>
    <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Tu fecha" required value="<?php echo $fecha; ?>">
  </div>
 

    
  <div class="form-group">
				    <label class="col-sm-2 control-label">Tipo de Ingreso</label>
				    <div class="col-sm-10">

<select name="categorias_id" class="form-control" >
				      	<option value="">Selecione un tipo de ingreso</option>
				      	<?php foreach ($filas as $filas): ?>
				      		<option value="<?php echo $filas->id; ?>"><?php echo $filas->nombre; ?></option>
				      	<?php endforeach ?>
				      </select>
</div>
				  </div>

  
 
   
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>