<?php

  include './models/categorias.php';
  $title="Listado de Usuarios";

  $categoria     = new Categorias();
  $id       = isset($_GET['id'])?$_GET['id']:null;
  $categorias    = $categoria->getCategoriaById($id);
  $nombre     = '';

 
  if($categorias){
    $nombre     =$categorias[0]['nombre'];

  
  }

	include 'toolbar.categorias.php';
?>
<form action="./controllers/categorias_controller.php" method="POST">
  <div class="form-group">
  	 <label for="nombre">Nombre</label>
    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Tus nombre" autofocus required value="<?php echo $nombre; ?>">
  </div>

  
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>