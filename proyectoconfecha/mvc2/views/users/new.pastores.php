<?php
	include 'toolbar.pastores.php';
?>
<form action="./controllers/pastores_controller.php" method="POST">
  <div class="form-group">
  
  	 <label for="identificacion">Identificacion</label>
    <input type="text" class="form-control" id="identificacion" name="identificacion" autofocus placeholder="Tu identificacion" required>
  </div>
  <div class="form-group">
  	 <label for="descripcion">Descripcion</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Tu descripcion" required>
  </div>
  <div class="form-group">
  	 <label for="monto">Monto</label>
    <input type="text" class="form-control" id="monto" name="monto" placeholder="Tu monto" required>
  </div>

  
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				El pastore ha sido creado.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear el pastore, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
</form>