<?php

  include './models/egresos.php';
  $title="Listado de Usuarios";
  
  $db=new PDO("mysql:host=localhost;dbname=crud2;charset=utf8","root","");
  $filas=$db->query("SELECT * FROM `categorias2`")->fetchAll(PDO::FETCH_OBJ);

  $egreso     = new Egresos();
  $id       = isset($_GET['id'])?$_GET['id']:null;
  $egresos    = $egreso->getEgresoById($id);
  $identificacion     = '';
  $descripcion = '';
  $monto    = '';
  $fecha    = '';
  $categorias2_id    = '';
 
  if($egresos){
    $identificacion     =$egresos[0]['identificacion'];
    $descripcion =$egresos[0]['descripcion'];
    $monto    =$egresos[0]['monto'];
    $fecha    =$egresos[0]['fecha'];
    $categorias2_id    =$egresos[0]['categorias2_id'];
  
  }

	include 'toolbar.egresos.php';
?>
<form action="./controllers/egresos_controller.php" method="POST">
  <div class="form-group">
  	 <label for="identificacion">Identificacion</label>
    <input type="text" class="form-control" id="identificacion" name="identificacion" placeholder="Tus identificacion" autofocus required value="<?php echo $identificacion; ?>">
  </div>
  <div class="form-group">
  	 <label for="descripcion">Nombre</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Tus descripcion" required value="<?php echo $descripcion; ?>">
  </div>
  <div class="form-group">
  	 <label for="monto">Monto</label>
    <input type="text" class="form-control" id="monto" name="monto" placeholder="Tu monto" required value="<?php echo $monto; ?>">
  </div>
 
  <div class="form-group">
  	 <label for="fecha">Fecha</label>
    <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Tu fecha" required value="<?php echo $fecha; ?>">
  </div>
 
  
  <div class="form-group">
				    <label class="col-sm-2 control-label">Tipo de Egreso</label>
				    <div class="col-sm-10">

<select name="categorias2_id" class="form-control" >
				      	<option value="">Selecione un tipo de Egreso</option>
				      	<?php foreach ($filas as $filas): ?>
				      		<option value="<?php echo $filas->id; ?>"><?php echo $filas->nombre; ?></option>
				      	<?php endforeach ?>
				      </select>
</div>
				  </div>
 
 
 
 
  
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>