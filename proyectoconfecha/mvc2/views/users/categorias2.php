<?php

	include './models/categorias2.php';
	$categoria2  = new Categorias2();

	//Si utiliza el filtro de busqueda
	if(isset($search)){
	    $categorias2 = $categoria2->getCategorias2BySearch($dataSearch);
	}else{
		//Trae todos las categorias
		$dataSearch=NULL;
		$categorias2 =$categoria2->getCategorias2();
	}

	$title="Listado de Categorias";
	include 'toolbar.categorias2.php';
?>
<div class="row">
	<div class="col text-center">
		<i class="material-icons" style="font-size: 80px;">people</i>
	</div>
</div>
<div class="row">
	<div class="col">
		<form action="./categorias2.php" method="post" accept-charset="utf-8" class="form-inline">
			<div class="form-group mx-sm-3 mb-2">
    			<input type="text" class="form-control" name="dataSearch" autofocus required placeholder="Buscar" value="<?php echo $dataSearch;  ?>">
  			</div>
  			<button type="submit" name="btnSearch" class="btn btn-primary mb-2">Buscar</button>
		</form>
	</div>
</div>
<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead class="thead-dark">
				<th>Id</th>
				<th class="text-center">Nombre</th>
				
				
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</thead>
			<tbody>
				<?php

					if(count($categorias2)>0){

						foreach ($categorias2 as $column =>$value) {
				?>

							<tr id="row<?php echo $value['id']; ?>">
								<td><?php echo $value['id']; ?></td>
								<td><?php echo $value['nombre']; ?></td>
								
								
								<td class="text-center">
									<a href="./categorias2.php?page6=edit.categorias2&id=<?php echo $value['id'] ?>" title="Editar usuario: <?php echo $value['id'].' '.$value['nombre'] ?>">
										<i class="material-icons btn_edit">edit</i>
									</a>
								</td>
								<td class="text-center">
									<a href="#" onclick="btnDeleteCategoria2(<?php echo $value["id"] ?>)" id="btnDeletecategoria2" title="Borrar usuario: <?php echo $value['id'].' '.$value['nombre'] ?>">
										<i class="material-icons btn_delete">delete_forever</i>
									</a>
								</td>
							</tr>
				<?php
						}
					}else{
				?>
					<tr>
						<td colspan="5">
							<div class="alert alert-info">
								No se encontraron usuarios.
							</div>
						</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col">
			<div class="alert alert-success" id="msgSuccess" style="display: none;"></div>
			<div class="alert alert-danger" id="msgDanger" style="display: none;"></div>
		</div>
	</div>
<script type="text/javascript">

	function btnDeleteCategoria2(id){
		if(confirm("Esta seguro de eliminar la categoria?")){
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function(){
			if (this.readyState == 4 && this.status == 200) {
				var response   = JSON.parse(this.responseText);
				var msgSuccess = document.getElementById('msgSuccess');
				var msgDanger   = document.getElementById('msgDanger');
				if(response.success){
					// alert("El usuario ha sido borrado de la base de datos.");
					msgSuccess.style.display = 'inherit';
					msgSuccess.innerHTML     = 'La categoria ha sido borrado de la base de datos.';
					msgDanger.style.display  = 'none';

					//Elimina el registro de la tabla
					var row    = document.getElementById('row'+id);
					var parent = row.parentElement;
        			parent.removeChild(row);

					// location.reload(true);
				}else if(response.error){
					// alert("No se ha podido eliminar el registro");
					msgDanger.style.display  = 'inherit';
					msgDanger.innerHTML      = 'No se ha podido eliminar la categoria';
					msgSuccess.style.display = 'none';
				}
			}
			};
			xhttp.open("GET", "./controllers/categorias2_controller.php?delete=true&id="+id, true);
			xhttp.send();
		}
	}


</script>