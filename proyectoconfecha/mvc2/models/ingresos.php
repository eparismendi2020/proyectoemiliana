<?php




	include dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Ingresos
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos los usuarios registrados
		public function getIngresos()
		{
			$query  ="SELECT ingresos.id,ingresos.identificacion,ingresos.descripcion,ingresos.monto, ingresos.fecha,
            ingresos.categorias_id,categorias.nombre FROM ingresos,categorias WHERE categorias.id=ingresos.categorias_id";
			
			
			
			$result =mysqli_query($this->link,$query);
		
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}
		

	
	

		    //Crea un nuevo usuario
		    public function newIngreso($data){
		        $query  ="INSERT INTO ingresos (identificacion, descripcion, monto, fecha, categorias_id)
             VALUES ('".$data['identificacion']."','".$data['descripcion']."','".$data['monto']."','".$data['fecha']."',
                     '".$data['categorias_id']."')";
		        $result =mysqli_query($this->link,$query);
		        echo $result;
		        if(mysqli_affected_rows($this->link)>0){
		            return true;
		        }else{
		            return false;
		        }
		    }
		    
		    
		    

		

		//Obtiene el usuario por id
		public function getIngresoById($id=NULL){
			if(!empty($id)){
				$query  ="SELECT * FROM ingresos WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
				
			}else{
				return false;
			}
		}

		
	
		
		
		//Obtiene el usuario por id
		public function setEditIngreso($data){
			if(!empty($data['id'])){
				$query  ="UPDATE ingresos SET identificacion='".$data['identificacion']."',
                descripcion='".$data['descripcion']."', monto='".$data['monto']."',fecha='".$data['fecha']."',
              categorias_id='".$data['categorias_id']."' WHERE id=".$data['id'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra el usuario por id
		public function deleteIngreso($id=NULL){
			if(!empty($id)){
				$query  ="DELETE FROM ingresos WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Filtro de busqueda
		public function getIngresosBySearch($data=NULL){
			if(!empty($data)){
		
				$query  ="SELECT * FROM ingresos INNER JOIN categorias
                 ON ingresos.categorias_id = categorias.id

 WHERE identificacion LIKE'%".$data."%' OR descripcion LIKE'%".$data."%' OR 
                monto LIKE'%".$data."%' OR  fecha LIKE'%".$data."%' OR  nombre LIKE'%".$data."%'";
				
			
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}
	}
	
	

	
	
	