<?php
	include dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Egresos
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos los usuarios registrados
		public function getegresos()
		{
		
		    
		    $query  ="SELECT egresos.id,egresos.identificacion,egresos.descripcion,egresos.monto,egresos.fecha,
            egresos.categorias2_id,categorias2.nombre FROM egresos,categorias2 WHERE categorias2.id=egresos.categorias2_id";
		    
		    
		    
		    
			$result =mysqli_query($this->link,$query);
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}

		//Crea un nuevo usuario
		public function newEgreso($data){
			
		    $query  ="INSERT INTO egresos (identificacion, descripcion, monto, fecha, categorias2_id)
             VALUES ('".$data['identificacion']."','".$data['descripcion']."','".$data['monto']."', '".$data['fecha']."',
                     '".$data['categorias2_id']."')";
		    
		    
		    
			$result =mysqli_query($this->link,$query);
			if(mysqli_affected_rows($this->link)>0){
				return true;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function getegresoById($id=NULL){
			if(!empty($id)){
				$query  ="SELECT * FROM egresos WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function setEditegreso($data){
			if(!empty($data['id'])){
			    $query  ="UPDATE egresos SET identificacion='".$data['identificacion']."',
                descripcion='".$data['descripcion']."', monto='".$data['monto']."',  fecha='".$data['fecha']."',
              categorias2_id='".$data['categorias2_id']."' WHERE id=".$data['id'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra el usuario por id
		public function deleteegreso($id=NULL){
			if(!empty($id)){
				$query  ="DELETE FROM egresos WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Filtro de busqueda
		public function getegresosBySearch($data=NULL){
			if(!empty($data)){
			    
			    $query  ="SELECT * FROM egresos INNER JOIN categorias2
                 ON egresos.categorias2_id = categorias2.id
			        
 WHERE identificacion LIKE'%".$data."%' OR descripcion LIKE'%".$data."%' OR
                monto LIKE'%".$data."%' OR  fecha LIKE'%".$data."%' OR   nombre LIKE'%".$data."%'";
				
				
				
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}
	}