<?php
include dirname(__file__,2).'/models/categorias2.php';

$categorias2=new Categorias2();

//Request: creacion de nuevo usuario
if(isset($_POST['create']))
{
    if($categorias2->newCategoria2($_POST)){
        header('location: ../categorias2.php?page6=new.categorias2&success=true');
    }else{
        header('location: ../categorias2.php?page6=new.categorias2&error=true');
    }
}

//Request: editar usuario
if(isset($_POST['edit']))
{
    if($categorias2->setEditCategoria2($_POST)){
        header('location: ../categorias2.php?page6=edit.categorias2&id='.$_POST['id'].'&success=true');
    }else{
        header('location: ../categorias2.php?page6=edit.categorias2&id='.$_POST['id'].'&error=true');
    }
}

//Request: editar usuario
if(isset($_GET['delete']))
{
    if($categorias2->deleteCategoria2($_GET['id'])){
        // header('location: ../categorias2.php?page6=categorias2&success=true');
        echo json_encode(["success"=>true]);
    }else{
        // header('location: ../categorias2.php?page6=categorias2&&error=true');
        echo json_encode(["error"=>true]);
    }
}

?>