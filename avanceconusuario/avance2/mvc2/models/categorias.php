<?php
	include dirname(__file__,2)."/config/conexion.php";
	/**
	*
	*/
	class Categorias
	{
		private $conn;
		private $link;

		function __construct()
		{
			$this->conn   = new Conexion();
			$this->link   = $this->conn->conectarse();
		}

		//Trae todos los usuarios registrados
		public function getCategorias()
		{
			$query  ="SELECT * FROM categorias";
			$result =mysqli_query($this->link,$query);
			$data   =array();
			while ($data[]=mysqli_fetch_assoc($result));
			array_pop($data);
			return $data;
		}

		//Crea un nuevo usuario
		public function newCategoria($data){
			$query  ="INSERT INTO categorias (nombre) VALUES ('".$data['nombre']."')";
			$result =mysqli_query($this->link,$query);
			if(mysqli_affected_rows($this->link)>0){
				return true;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function getCategoriaById($id=NULL){
			if(!empty($id)){
				$query  ="SELECT * FROM categorias WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}

		//Obtiene el usuario por id
		public function setEditcategoria($data){
			if(!empty($data['id'])){
				$query  ="UPDATE categorias SET nombre='".$data['nombre']."' WHERE id=".$data['id'];
				$result =mysqli_query($this->link,$query);
				if($result){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Borra el usuario por id
		public function deletecategoria($id=NULL){
			if(!empty($id)){
				$query  ="DELETE FROM categorias WHERE id=".$id;
				$result =mysqli_query($this->link,$query);
				if(mysqli_affected_rows($this->link)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		//Filtro de busqueda
		public function getcategoriasBySearch($data=NULL){
			if(!empty($data)){
				$query  ="SELECT * FROM categorias WHERE nombre LIKE'%".$data."%'";
				$result =mysqli_query($this->link,$query);
				$data   =array();
				while ($data[]=mysqli_fetch_assoc($result));
				array_pop($data);
				return $data;
			}else{
				return false;
			}
		}
	}