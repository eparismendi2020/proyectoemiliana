<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-language" content="es">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title=isset($title)?$title:'SISTEMA DE INGRESOS Y EGRESOS'; ?></title>
	<link rel="stylesheet" type="text/css" href="./misc/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./misc/style.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style type="text/css">
		body{
			/*margin-top: 20px;*/
		}
	</style>
</head>
<body>
 <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Sistema Web de Ingresos y Egresos</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="paginaprincipal.php">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
        
         <li class="nav-item">
            <a class="nav-link" href="ingresos.php">Ingresos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="egresos.php">Egresos</a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="pastores.php">Pastores</a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="lista.reportes.php">Empleados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php">Usuarios</a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="categorias.php">Categorias</a>
          </li>
          
          
        </ul>
      </div>
    </div>
  </nav>
 <br> <br> <br>
	<div class="container">