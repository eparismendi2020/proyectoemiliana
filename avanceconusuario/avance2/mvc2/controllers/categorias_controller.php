<?php
include dirname(__file__,2).'/models/categorias.php';

$categorias=new Categorias();

//Request: creacion de nuevo usuario
if(isset($_POST['create']))
{
    if($categorias->newCategoria($_POST)){
        header('location: ../categorias.php?page5=new.categorias&success=true');
    }else{
        header('location: ../categorias.php?page5=new.categorias&error=true');
    }
}

//Request: editar usuario
if(isset($_POST['edit']))
{
    if($categorias->setEditCategoria($_POST)){
        header('location: ../categorias.php?page5=edit.categorias&id='.$_POST['id'].'&success=true');
    }else{
        header('location: ../categorias.php?page5=edit.categorias&id='.$_POST['id'].'&error=true');
    }
}

//Request: editar usuario
if(isset($_GET['delete']))
{
    if($categorias->deleteCategoria($_GET['id'])){
        // header('location: ../categorias.php?page5=categorias&success=true');
        echo json_encode(["success"=>true]);
    }else{
        // header('location: ../categorias.php?page5=categorias&&error=true');
        echo json_encode(["error"=>true]);
    }
}

?>