
<?php
	include dirname(__file__,2).'/models/egresos.php';

	$egresos=new Egresos();

	//Request: creacion de nuevo usuario
	if(isset($_POST['create']))
	{
		if($egresos->newEgreso($_POST)){
			header('location: ../egresos.php?page3=new.egresos&success=true');
		}else{
			header('location: ../egresos.php?page3=new.egresos&error=true');
		}
	}

	//Request: editar usuario
	if(isset($_POST['edit']))
	{
		if($egresos->setEditEgreso($_POST)){
			header('location: ../egresos.php?page3=edit.egresos&id='.$_POST['id'].'&success=true');
		}else{
			header('location: ../egresos.php?page3=edit.egresos&id='.$_POST['id'].'&error=true');
		}
	}

	//Request: editar usuario
	if(isset($_GET['delete']))
	{
		if($egresos->deleteegreso($_GET['id'])){
			// header('location: ../egresos.php?page3=egresos&success=true');
			echo json_encode(["success"=>true]);
		}else{
			// header('location: ../egresos.php?page3=egresos&&error=true');
			echo json_encode(["error"=>true]);
		}
	}

?>