<?php
	include 'toolbar.ingresos.php';
	$db=new PDO("mysql:host=localhost;dbname=crud2;charset=utf8","root","");
	$filas=$db->query("SELECT * FROM `categorias`")->fetchAll(PDO::FETCH_OBJ); ?>
	
	
	

<form action="./controllers/ingresos_controller.php"method="POST">
  <div class="form-group">
  
  	 <label for="identificacion">Identificacion</label>
    <input type="text" class="form-control" id="identificacion" name="identificacion" autofocus placeholder="Tu identificacion" required>
  </div>
  <div class="form-group">
  	 <label for="descripcion">Descripcion</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Tu descripcion" required>
  </div>
  <div class="form-group">
  	 <label for="monto">Monto</label>
    <input type="text" class="form-control" id="monto" name="monto" placeholder="Tu monto" required>
  </div>
  
  
  <div class="form-group">
				    <label class="col-sm-2 control-label">Tipo de Ingreso</label>
				    <div class="col-sm-10">

<select name="categorias_id" class="form-control">
				      	<option value="">Selecione un tipo de ingreso</option>
				      	<?php foreach ($filas as $filas): ?>
				      		<option value="<?php echo $filas->id; ?>"><?php echo $filas->nombre; ?></option>
				      	<?php endforeach ?>
				      </select>
</div>
				  </div>



  
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				El ingreso ha sido creado.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear el ingreso, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  
  
  
  
  
  
  
  
  	
  </div>
</form>