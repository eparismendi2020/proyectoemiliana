<?php

  include './models/pastores.php';
  $title="Listado de Usuarios";

  $pastore     = new Pastores();
  $id       = isset($_GET['id'])?$_GET['id']:null;
  $pastores    = $pastore->getPastoreById($id);
  $identificacion     = '';
  $descripcion = '';
  $monto    = '';
 
  if($pastores){
    $identificacion     =$pastores[0]['identificacion'];
    $descripcion =$pastores[0]['descripcion'];
    $monto    =$pastores[0]['monto'];
  
  }

	include 'toolbar.pastores.php';
?>
<form action="./controllers/pastores_controller.php" method="POST">
  <div class="form-group">
  	 <label for="identificacion">Identificacion</label>
    <input type="text" class="form-control" id="identificacion" name="identificacion" placeholder="Tus identificacion" autofocus required value="<?php echo $identificacion; ?>">
  </div>
  <div class="form-group">
  	 <label for="descripcion">Descripcion</label>
    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Tus descripcion" required value="<?php echo $descripcion; ?>">
  </div>
  <div class="form-group">
  	 <label for="monto">Monto</label>
    <input type="text" class="form-control" id="monto" name="monto" placeholder="Tu monto" required value="<?php echo $monto; ?>">
  </div>
 
  
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>