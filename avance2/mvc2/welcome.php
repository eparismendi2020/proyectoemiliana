
<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>














<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sistema Web de Ingresos y Egresos</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/full-width-pics.css" rel="stylesheet">

</head>

<body>
 

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
     <a href="logout.php" class="btn btn-danger">Cierra la sesión</a> <BR>
   <a href="reset-password.php" class="btn btn-warning">Cambia tu contraseña</a> <BR>
      <a class="navbar-brand" href="#"> <BR> &nbsp  &nbsp  Sistema Web de Ingresos y Egresos</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="welcome.php">Inicio
              <span class="sr-only">(current)</span>
            </a>
          </li>
        
         <li class="nav-item">
            <a class="nav-link" href="ingresos.php">Ingresos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="egresos.php">Egresos</a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="pastores.php">Pastores</a>
             <li class="nav-item">
            <a class="nav-link" href="categorias.php"> <center> Reportes   </center></a>
          </li>
          
          </li>
          
          
           
           <li class="nav-item">
            <a class="nav-link" href="categorias.php"> <center> Categorias Ingresos  </center></a>
          </li>
          
          
          
           <li class="nav-item">
            <a class="nav-link" href="categorias.php"> <center> Categorias Egresos   </center></a>
          </li>
        
          
        </ul>
        
        
        
       
      </div>
    </div>
  </nav>
     

  <!-- Header - set the background image for the header in the line below -->
  <header class="py-5 bg-image-full" style="background-image: url('https://images3.alphacoders.com/747/thumb-1920-747054.jpg');">
    <img class="img-fluid d-block mx-auto" src="img/logo.jpg" alt="">
  </header>

  <!-- Content section -->
  <section class="py-5">
    <div class="container">
      <h1>SISTEMA INFORMATICO ENCARGADO DE INGRESO Y EGRESOS MONETARIO DE LA IGLESIA CUADRANGULAR</h1>
      <p class="lead">Esta es una denominacion eminentemente pentecostal en su doctrina y forma de adoracion. La Iglesia Cuadrangular se llama asi porque la doctrina de la Iglesia reconoce 4 aspectos principales de la Obra y la persona de Jesucristo.</p>
      
    </div>
  </section>


 <!-- Image Section - set the background image for the header in the line below -->
 <section class="py-5 bg-image-full" style="background-image: url('https://i.ibb.co/KxDMfPq/Presentaci-n1.jpg');">
  <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
  <div style="height: 200px;"></div>
</section>


























  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Desarrollado por: <BR> Emiliana Arismendi, Edwin Baja�a, Jorge Cajape 
                                            Julissa Candelario , Nicole Vasquez <br>
                                          ESTUDIANTES 4TO SEMESTRE CURSO F</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
